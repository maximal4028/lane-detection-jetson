# Зависимости

Cuda<br>
TensorRT 8<br>
opencv<br>
cmake<br>

# Запуск модели на Jetson

Настройте пусти в файле CmakeList.txt на свои. Также заменить путь к onnx модели в файле **lane.cpp**

## Создайте папку **build**
```
mkdir build
```

## Инициализируйте Cmake
```
cd build
cmake ..
make
```

## Для генерации engine используйте команду
```
unet -s
```
Затем заменить в файле **lane.cpp** путь для engine файла и выполните команду для предсказания изображений в папке<br>
```
unet -d ../samples
```

