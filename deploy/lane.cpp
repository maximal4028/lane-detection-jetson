#include <iostream>
#include <chrono>
#include "cuda_runtime_api.h"
#include "logging.h"
#include "NvOnnxParser.h"
#include "NvInfer.h"
#include <opencv2/opencv.hpp>
#include <dirent.h>
#include <vector>
#include <fstream>
#include <map>
#include <sstream>
#include <math.h>
#include <assert.h>

#define DEVICE 0
#define BATCH_SIZE 1

#define CHECK(status) \
    do\
    {\
        auto ret = (status);\
        if (ret != 0)\
        {\
            std::cerr << "Cuda failure: " << ret << std::endl;\
            abort();\
        }\
    } while (0)

using namespace nvinfer1;

static const int INPUT_H = 480;
static const int INPUT_W = 640;
static const int OUTPUT_SIZE = 640*480;
static const int NUM_CLASSES = 8;

const char* INPUT_BLOB_NAME = "input";
const char* OUTPUT_BLOB_NAME = "output";

std::string model_path = "/home/sensum/tregubenko_ssd/unet_onnx/models/unet_resnet34_8classes.onnx";
std::string engine_name = "unet_resnet34_8classes.engine";

using namespace nvinfer1;

static Logger gLogger;

struct TRTDestroy
{
    template< class T >
    void operator()(T* obj) const
    {
        if (obj)
        {
            obj->destroy();
        }
    }
};

template< class T >
using TRTUniquePtr = std::unique_ptr< T, TRTDestroy >;

void parseOnnxModel(const std::string& model_path, IHostMemory** modelStream)
{
    
    auto builder = TRTUniquePtr<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(gLogger));

    const auto explicitBatch = 1U << static_cast<uint32_t>(NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);
    auto network = TRTUniquePtr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(explicitBatch));
    auto config = TRTUniquePtr<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());

    auto parser = TRTUniquePtr<nvonnxparser::IParser>(nvonnxparser::createParser(*network, gLogger));

    if (!parser->parseFromFile(model_path.c_str(), static_cast< int >(nvinfer1::ILogger::Severity::kINFO)))
    {
        std::cerr << "ERROR: could not parse the model.\n";
        return;
    }

    config->setMaxWorkspaceSize(16 * (1 << 20)); 
    config->setFlag(nvinfer1::BuilderFlag::kFP16);
    builder->setMaxBatchSize(1);

    std::cout << "Building engine, please wait for a while..." << std::endl;
    auto engine = TRTUniquePtr< nvinfer1::ICudaEngine >(builder->buildEngineWithConfig(*network, *config));
    std::cout << "Build engine successfully!" << std::endl;


    (*modelStream) = engine->serialize();

}

cv::Mat preprocess_img(cv::Mat& img) {
    int w, h, x, y;
    float r_w = INPUT_W / (img.cols*1.0);
    float r_h = INPUT_H / (img.rows*1.0);
    if (r_h > r_w) {
        w = INPUT_W;
        h = r_w * img.rows;
        x = 0;
        y = (INPUT_H - h) / 2;
    } else {
        w = r_h* img.cols;
        h = INPUT_H;
        x = (INPUT_W - w) / 2;
        y = 0;
    }
    cv::Mat re(h, w, CV_8UC3);
    cv::resize(img, re, re.size(), 0, 0, cv::INTER_CUBIC);
    cv::Mat out(INPUT_H, INPUT_W, CV_8UC3, cv::Scalar(128, 128, 128));
    re.copyTo(out(cv::Rect(x, y, re.cols, re.rows)));
    return out;
}


void doInference(IExecutionContext& context, float* input, float* output, int batchSize) {
    const ICudaEngine& engine = context.getEngine();

    assert(engine.getNbBindings() == 2);
    void* buffers[2];

    const int inputIndex = engine.getBindingIndex(INPUT_BLOB_NAME);
    const int outputIndex = engine.getBindingIndex(OUTPUT_BLOB_NAME);

    CHECK(cudaMalloc(&buffers[inputIndex], batchSize * 3 * INPUT_H * INPUT_W * sizeof(float)));
    CHECK(cudaMalloc(&buffers[outputIndex], batchSize * OUTPUT_SIZE * NUM_CLASSES * sizeof(float)));

    cudaStream_t stream;
    CHECK(cudaStreamCreate(&stream));

    CHECK(cudaMemcpyAsync(buffers[inputIndex], input, batchSize * 3 * INPUT_H * INPUT_W * sizeof(float), cudaMemcpyHostToDevice, stream));
    context.enqueue(batchSize, buffers, stream, nullptr);
    CHECK(cudaMemcpyAsync(output, buffers[outputIndex], batchSize * OUTPUT_SIZE * NUM_CLASSES * sizeof(float), cudaMemcpyDeviceToHost, stream));

    cudaStreamSynchronize(stream);


    cudaStreamDestroy(stream);
    CHECK(cudaFree(buffers[inputIndex]));
    CHECK(cudaFree(buffers[outputIndex]));
}

struct  Detection{        
    int *mask = new int[INPUT_W*INPUT_H];
    };

float sigmoid(float x){
    return (1 / (1 + exp(-x)));
}

void softmax(double* input, size_t size) {

	assert(0 <= size <= sizeof(input) / sizeof(double));

	int i;
	double m, sum, constant;

	m = -INFINITY;
	for (i = 0; i < size; ++i) {
		if (m < input[i]) {
			m = input[i];
		}
	}

	sum = 0.0;
	for (i = 0; i < size; ++i) {
		sum += exp(input[i] - m);
	}

	constant = m + log(sum);
	for (i = 0; i < size; ++i) {
		input[i] = exp(input[i] - constant);
	}

}

void process_cls_result(Detection &res, float *output) {    
    for(int i=0;i<INPUT_W*INPUT_H;i++){
        res.mask[i] = sigmoid(*(output+i));
        double output_classes[NUM_CLASSES];
        for(int c=0;c<NUM_CLASSES;c++){
            output_classes[c] = double(*(output+(OUTPUT_SIZE*c+i)));
        }
        softmax(output_classes, NUM_CLASSES);
        int class_num = std::distance(output_classes, std::max_element(output_classes, output_classes + NUM_CLASSES));
        res.mask[i] = class_num;
    }

}

int read_files_in_dir(const char *p_dir_name, std::vector<std::string> &file_names) {
    DIR *p_dir = opendir(p_dir_name);
    if (p_dir == nullptr) {
        return -1;
    }

    struct dirent* p_file = nullptr;
    while ((p_file = readdir(p_dir)) != nullptr) {
        if (strcmp(p_file->d_name, ".") != 0 &&
                strcmp(p_file->d_name, "..") != 0) {
            std::string cur_file_name(p_file->d_name);
            file_names.push_back(cur_file_name);
        }
    }

    closedir(p_dir);
    return 0;
}

int main(int argc, char** argv) {
    cudaSetDevice(DEVICE);
    char *trtModelStream{nullptr};
    size_t size{0};
    if (argc == 2 && std::string(argv[1]) == "-s") {
        IHostMemory* modelStream{nullptr};
        parseOnnxModel(model_path, &modelStream);
        assert(modelStream != nullptr);
        std::ofstream p(engine_name, std::ios::binary);
        if (!p) {
            std::cerr << "could not open plan output file" << std::endl;
            return -1;
        }
        p.write(reinterpret_cast<const char*>(modelStream->data()), modelStream->size());
        modelStream->destroy();
        return 0;
    } else if (argc == 3 && std::string(argv[1]) == "-d") {
        std::ifstream file(engine_name, std::ios::binary);
        if (file.good()) {
            file.seekg(0, file.end);
            size = file.tellg();
            file.seekg(0, file.beg);
            trtModelStream = new char[size];
            assert(trtModelStream);
            file.read(trtModelStream, size);
            file.close();
        }
    } else {
        std::cerr << "arguments not right!" << std::endl;
        std::cerr << "./unet -s  // serialize model to plan file" << std::endl;
        std::cerr << "./unet -d ../samples  // deserialize plan file and run inference" << std::endl;
        return -1;
    }

    std::vector<std::string> file_names;
    if (read_files_in_dir(argv[2], file_names) < 0) {
        std::cout << "read_files_in_dir failed." << std::endl;
        return -1;
    }

    static float data[BATCH_SIZE * 3 * INPUT_H * INPUT_W];
    static float prob[BATCH_SIZE * OUTPUT_SIZE * NUM_CLASSES];
    IRuntime* runtime = createInferRuntime(gLogger);
    assert(runtime != nullptr);
    ICudaEngine* engine = runtime->deserializeCudaEngine(trtModelStream, size);
    assert(engine != nullptr);
    IExecutionContext* context = engine->createExecutionContext();
    assert(context != nullptr);
    delete[] trtModelStream;

    int fcount = 0;
    for (int f = 0; f < (int)file_names.size(); f++) {
        fcount++;
        if (fcount < BATCH_SIZE && f + 1 != (int)file_names.size()) continue;
        for (int b = 0; b < fcount; b++) {
            cv::Mat img = cv::imread(std::string(argv[2]) + "/" + file_names[f - fcount + 1 + b]);
            if (img.empty()) continue;
            cv::Mat pr_img = preprocess_img(img);
            int i = 0;
            for (int row = 0; row < INPUT_H; ++row) {
                uchar* uc_pixel = pr_img.data + row * pr_img.step;
                for (int col = 0; col < INPUT_W; ++col) {
                    data[b * 3 * INPUT_H * INPUT_W + i] = (float)uc_pixel[2] / 255.0;
                    data[b * 3 * INPUT_H * INPUT_W + i + INPUT_H * INPUT_W] = (float)uc_pixel[1] / 255.0;
                    data[b * 3 * INPUT_H * INPUT_W + i + 2 * INPUT_H * INPUT_W] = (float)uc_pixel[0] / 255.0;
                    uc_pixel += 3;
                    ++i;
                }
            }
        }

        auto start = std::chrono::system_clock::now();
        doInference(*context, data, prob, BATCH_SIZE);
        auto end = std::chrono::system_clock::now();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
        
        std::vector<Detection> batch_res(fcount);
        for (int b = 0; b < fcount; b++) {
             auto& res = batch_res[b];
             process_cls_result(res, &prob[b * OUTPUT_SIZE]);
         }

        for (int b = 0; b < fcount; b++) {
            auto& res = batch_res[b];
            int * mask = res.mask;
            cv::Mat mask_mat(INPUT_H, INPUT_W, CV_8UC3, cv::Scalar(0, 0, 0));
            for(int h =0; h< INPUT_H ;h++){
                for(int w=0;w<INPUT_W;w++){
                    int * pixcel = mask+h*INPUT_W+w;
                    switch(*pixcel)
                    {
                    case 1:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 0;
                        break;
                    case 2:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 0;
                        break;
                    case 3:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 255;
                        break;
                    case 4:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 0;
                        break;
                    case 5:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 255;
                        break;
                    case 6:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 0;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 255;
                        break;
                    case 7:
                        mask_mat.at<cv::Vec3b>(h,w)[0] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[1] = 255;
                        mask_mat.at<cv::Vec3b>(h,w)[2] = 255;
                        break;
                    default:
                        break;
                    }
                }

            }
            cv::Mat img = cv::imread(std::string(argv[2]) + "/" + file_names[f - fcount + 1 + b]);
            double alpha = 0.25; double beta; double input;
            beta = 1.0 - alpha;
            cv::Mat dst;
            cv::addWeighted(mask_mat, alpha, img, beta, 0.0, dst);
            cv::imwrite("s_" + file_names[f - fcount + 1 + b] + "_unet.jpg", dst); 
        }
         for (int b = 0; b < fcount; b++){
            delete [] batch_res[b].mask;
         }
         
         fcount = 0;
    }

    context->destroy();
    engine->destroy();
    runtime->destroy();

    return 0;
}
