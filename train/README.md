# Обучение на Python

## Установка зависимостей

1. [Install CUDA](https://developer.nvidia.com/cuda-downloads)

2. [Install PyTorch](https://pytorch.org/get-started/locally/)

3. Install dependencies
```bash
pip install -r requirements.txt
```

## Запуск
**Note : Use Python 3.6 or newer**

### Обучение

Пример компанды для обучения 

`python train.py -c 8 -e 100 -b 8 --scale 1 --amp -v 15` 

```console
> python train.py -h
usage: train.py [-h] [--epochs E] [--batch-size B] [--learning-rate LR]
                [--load LOAD] [--scale SCALE] [--validation VAL] [--amp]

optional arguments:
  -h, --help            show this help message and exit
  --epochs E, -e E      Number of epochs
  --batch-size B, -b B  Batch size
  --learning-rate LR, -l LR
                        Learning rate
  --load LOAD, -f LOAD  Load model from a .pth file
  --scale SCALE, -s SCALE
                        Downscaling factor of the images
  --validation VAL, -v VAL
                        Percent of the data that is used as validation (0-100)
  --amp                 Use mixed precision
  -c                    Number of classes
```

### Предикт

После обучения используйте сохраненные веса модели `MODEL.pth`.

Для предсказания одного изображения используйте команду:

`python predict.py -m /path/to/model.pth -i image.jpg -o output.jpg` 

Для предсказания нескольких изображений используйте команду:

`python predict.py -m /path/to/model.pth -i image1.jpg image2.jpg`

Для предсказания видео используйте команду:

`python predict.py -m /path/to/model.pth -i video.mp4 --video -o video_out.mp4 -s 1 -c 8`


```console
> python predict.py -h
usage: predict.py [-h] [--model FILE] --input INPUT [INPUT ...] 
                  [--output INPUT [INPUT ...]] [--viz] [--no-save]
                  [--mask-threshold MASK_THRESHOLD] [--scale SCALE]

Predict masks from input images

optional arguments:
  -h, --help            show this help message and exit
  --model FILE, -m FILE
                        Specify the file in which the model is stored
  --input INPUT [INPUT ...], -i INPUT [INPUT ...]
                        Filenames of input images
  --output INPUT [INPUT ...], -o INPUT [INPUT ...]
                        Filenames of output images
  --viz, -v             Visualize the images as they are processed
  --no-save, -n         Do not save the output masks
  --mask-threshold MASK_THRESHOLD, -t MASK_THRESHOLD
                        Minimum probability value to consider a mask pixel white
  --scale SCALE, -s SCALE
                        Scale factor for the input images
  --video               For video output
  --onnx                Save ONNX format
  --onnx-path           Path to save onnx
```
You can specify which model file to use with `--model MODEL.pth`.

### ONNX

Для того, чтобы использовать портировать модель на Jetson, необходимо перевести веса в onnx формат. Для этого используйте команду:

`python predict.py -m /path/to/model.pth -i image1.jpg -o output.jpg --onnx --onnx-path /path/to/save/model.onnx -s 1 -c 8`

