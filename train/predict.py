import argparse
import logging
import os

import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms

from utils.data_loading import BasicDataset
from unet import UNet
from utils.utils import plot_img_and_mask

import segmentation_models_pytorch as smp
import cv2

def predict_img(net,
                full_img,
                device,
                scale_factor=1,
                out_threshold=0.5):
    net.eval()
    img = torch.from_numpy(BasicDataset.preprocess(full_img, scale_factor, is_mask=False))
    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img)

        if args.classes > 1:
            probs = F.softmax(output, dim=1)[0]
        else:
            probs = torch.sigmoid(output)[0]

        # tf = transforms.Compose([
        #     transforms.ToPILImage(),
        #     transforms.Resize((full_img.size[1], full_img.size[0])),
        #     transforms.ToTensor()
        # ])

        # full_mask = tf(probs.cpu()).squeeze()
        full_mask = probs.cpu().squeeze()

    if args.classes == 1:
        return (full_mask > out_threshold).numpy()
    else:
        return F.one_hot(full_mask.argmax(dim=0), args.classes).permute(2, 0, 1).numpy()


def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images')
    parser.add_argument('--model', '-m', default='MODEL.pth', metavar='FILE',
                        help='Specify the file in which the model is stored')
    parser.add_argument('--input', '-i', metavar='INPUT', nargs='+', help='Filenames of input images', required=True)
    parser.add_argument('--output', '-o', metavar='OUTPUT', nargs='+', help='Filenames of output images')
    parser.add_argument('--viz', '-v', action='store_true',
                        help='Visualize the images as they are processed')
    parser.add_argument('--no-save', '-n', action='store_true', help='Do not save the output masks')
    parser.add_argument('--mask-threshold', '-t', type=float, default=0.5,
                        help='Minimum probability value to consider a mask pixel white')
    parser.add_argument('--scale', '-s', type=float, default=0.5,
                        help='Scale factor for the input images')
    parser.add_argument('--bilinear', action='store_true', default=False, help='Use bilinear upsampling')
    parser.add_argument('--classes', '-c', type=int, default=8, help='Number of classes')
    parser.add_argument('--video', action='store_true', default=False, help='Is it video?')
    parser.add_argument('--onnx', action='store_true', default=False, help='Save ONNX format')
    parser.add_argument('--onnx-path', default='Model.onnx', help='Path to save onnx')

    return parser.parse_args()


def get_output_filenames(args):
    def _generate_name(fn):
        split = os.path.splitext(fn)
        return f'{split[0]}_OUT{split[1]}'

    return args.output or list(map(_generate_name, args.input))

def save_onnx(model, full_img, save_path):
    input_names = ["input"]
    output_names = ["output"]

    img = torch.from_numpy(BasicDataset.preprocess(full_img, 1, is_mask=False))
    img = img.unsqueeze(0)
    dummy_input = img.to(device=device, dtype=torch.float32)
    
    torch.onnx.export(model, 
                  dummy_input,
                  save_path,
                  verbose=False,
                  input_names=input_names,
                  output_names=output_names,
                  export_params=True,
                  opset_version=11
                  )

    return save_path


def mask_to_image(mask: np.ndarray, img: np.ndarray, img_height: int, img_width: int, is_video: bool):

    unique_values = [[0, 0, 0],
                    [255, 0, 0],
                    [0, 255, 0],
                    [0, 0, 255],
                    [255, 255, 0],
                    [255, 0, 255],
                    [0, 255, 255],
                    [255, 255, 255]]

    color_seg = np.zeros((mask.shape[1], mask.shape[2], 3),
                         dtype=np.uint8)

    lane_out = np.zeros(
        (mask.shape[1], mask.shape[2]))

    for label, color in enumerate(unique_values):
        # lane_out[:][np.all(mask[label] == 1, axis=2)] = i
        lane_out[:][mask[label,:] == 1] = label
        color_seg[lane_out == label, :] = color

    alpha = 0.3

    color_seg = cv2.cvtColor(color_seg, cv2.COLOR_BGR2RGB)
    
    mixed_img = cv2.addWeighted(color_seg, alpha, img.reshape((480, 640, 3)), 1 - alpha, 0, dtype = cv2.CV_32F)

    mixed_img = cv2.resize(mixed_img, (img_width, img_height), interpolation = cv2.INTER_LINEAR)


    if is_video:
        return mixed_img.astype(np.uint8)

    else:
        return Image.fromarray(mixed_img.astype(np.uint8))

    # if mask.ndim == 2:
    #     return Image.fromarray((mask * 255).astype(np.uint8))
    # elif mask.ndim == 3:
    #     return Image.fromarray((np.argmax(mask, axis=0) * 255 / mask.shape[0]).astype(np.uint8))


if __name__ == '__main__':
    args = get_args()
    in_files = args.input
    out_files = get_output_filenames(args)

    net = smp.Unet(
    encoder_name="resnet34",        # choose encoder, e.g. mobilenet_v2 or efficientnet-b7
    in_channels=3,                  # model input channels (1 for gray-scale images, 3 for RGB, etc.)
    classes=8,                      # model output channels (number of classes in your dataset)
)


    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info(f'Loading model {args.model}')
    logging.info(f'Using device {device}')

    net.to(device=device)
    net.load_state_dict(torch.load(args.model, map_location=device))

    logging.info('Model loaded!')
    if args.video:
        cap = cv2.VideoCapture(args.input[0])

        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        frame_size = (frame_width,frame_height)
        fps = cap.get(5)

        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        out = cv2.VideoWriter(args.output[0], fourcc, fps, frame_size)

        while(cap.isOpened()):
            ret, img = cap.read()

            img_resized = cv2.resize(np.array(img), (640, 480), interpolation = cv2.INTER_AREA)

            mask = predict_img(net=net,
                        full_img=Image.fromarray(np.uint8(img_resized)),
                        scale_factor=args.scale,
                        out_threshold=args.mask_threshold,
                        device=device)
            
            result = mask_to_image(mask, img_resized, frame_height, frame_width, args.video)
            out.write(result)

        cap.release()
        out.release()


    else:
        for i, filename in enumerate(in_files):
            logging.info(f'\nPredicting image {filename} ...')
            img = Image.open(filename)
            input_height = np.array(img).shape[0]
            input_width = np.array(img).shape[1]
            
            img_resized = cv2.resize(np.array(img), (640, 480), interpolation = cv2.INTER_AREA)

            mask = predict_img(net=net,
                            full_img=Image.fromarray(np.uint8(img_resized)),
                            scale_factor=args.scale,
                            out_threshold=args.mask_threshold,
                            device=device)
            
            if args.onnx:
                save_path = save_onnx(net, Image.fromarray(np.uint8(img_resized)), args.onnx_path)

            if not args.no_save:
                out_filename = out_files[i]
                result = mask_to_image(mask, img_resized, input_height, input_width, args.video)
                result.save(out_filename)
                logging.info(f'Mask saved to {out_filename}')

            if args.viz:
                logging.info(f'Visualizing results for image {filename}, close to continue...')
                plot_img_and_mask(img, mask)
