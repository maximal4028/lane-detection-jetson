# lane-detection-jetson

Lane detection for Jetson

![Figure_1.png](data/scene_1_20160512_1329_00_000181.png?raw=true)

## Getting started

Инструкции по обучению модели находятся в папке **train**

Инструкции по портированию на Jetson находятся в папке **deploy**
